package qa.stepdefinitions;

import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.cucumber.spring.CucumberContextConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.testng.Assert;
import qa.conf.DriverConfig;
import qa.tasks.ImportarTarifa;
import qa.tasks.MenuListarTarifa;
import qa.tasks.NavigateTo;
import qa.tasks.ValidarImportarTarifa;

@CucumberContextConfiguration
@ContextConfiguration(classes = {DriverConfig.class})
public class ImportarTarifaSepDefs {

    @Autowired
    private NavigateTo navigateTo;

    @Autowired
    private MenuListarTarifa menuListarTarifa;

    @Autowired
    private ImportarTarifa importarTarifa;

    @Autowired
    private ValidarImportarTarifa validarImportarTarifa;

    @Before
    public void goHomePage() throws InterruptedException {
        navigateTo.homePage();
        Thread.sleep(1000);
    }

    @Given("actuario quiere importar tarifa")
    public void actuario_quiere_importar_tarifa() throws InterruptedException {
        menuListarTarifa.irMenuListarTarifa();
        Thread.sleep(1000);
    }

    @When("^actuario presiona importar tipo tarifa (.+)$")
    public void actuario_presiona_importar_tipo_tarifa(String tarifa) throws Throwable {
        importarTarifa.withCorrectFile(tarifa);
        Thread.sleep(1000);
    }

    @Then("^actuario tiene tipo tarifa (.+) creada$")
    public void actuario_tiene_tipo_tarifa_creada(String tarifa) throws Throwable {
        Assert.assertTrue(validarImportarTarifa.validarWithCorrectFile(tarifa));
        System.out.println("UPLOAD");
    }

    @When("^actuario presiona importar con formato incorrecto tipo tarifa (.+)$")
    public void actuario_presiona_importar_con_formato_incorrecto_tipo_tarifa(String tarifa) throws Throwable {
        importarTarifa.withIncorrectFormatFile(tarifa);
        Thread.sleep(1000);
    }

    @Then("^actuario no tiene tipo tarifa (.+) creada con formato incorrecto$")
    public void actuario_no_tiene_tipo_tarifa_creada_con_formato_incorrecto(String tarifa) throws Throwable {
        Assert.assertTrue(validarImportarTarifa.validarWithIncorrectFormatFile(tarifa));
        System.out.println("UPLOAD");
    }

    @When("^actuario presiona importar con celdas vacias tipo tarifa (.+)$")
    public void actuario_presiona_importar_con_celdas_vacias_tipo_tarifa(String tarifa) throws Throwable {
        importarTarifa.withEmptyFieldsFile(tarifa);
        Thread.sleep(1000);
    }

    @Then("^actuario no tiene tipo tarifa (.+) creada con celdas vacias$")
    public void actuario_no_tiene_tipo_tarifa_creada_con_celdas_vacias(String tarifa) throws Throwable {
        Assert.assertTrue(validarImportarTarifa.validarEmptyFieldsFile(tarifa));
        System.out.println("UPLOAD");
    }

    @When("^actuario presiona importar con con grupo tarifario version tipo tarifa (.+)$")
    public void actuario_presiona_importar_con_con_grupo_tarifario_version_tipo_tarifa(String tarifa) throws Throwable {
        importarTarifa.withRateGroupVersion(tarifa);
        Thread.sleep(1000);
    }

    @Then("^actuario no tiene tipo tarifa (.+) creada con grupo tarifario version$")
    public void actuario_no_tiene_tipo_tarifa_creada_con_grupo_tarifario_version(String tarifa) throws Throwable {
        Assert.assertTrue(validarImportarTarifa.validarWithRateGroupVersion(tarifa));
        System.out.println("UPLOAD");
    }

    @When("^actuario presiona importar con modelo no existente tipo tarifa (.+)$")
    public void actuario_presiona_importar_con_modelo_no_existente_tipo_tarifa(String tarifa) throws Throwable {
        importarTarifa.withOutModeloNonExistent(tarifa);
        Thread.sleep(1000);
    }

    @Then("^actuario no tiene tipo tarifa (.+) creada con modelo no existente$")
    public void actuario_no_tiene_tipo_tarifa_creada_con_modelo_no_existente(String tarifa) throws Throwable {
        Assert.assertTrue(validarImportarTarifa.validarWithOutModeloNonExistent(tarifa));
        System.out.println("UPLOAD");
    }

    @When("^actuario presiona importar con cobertura no existente tipo tarifa (.+)$")
    public void actuario_presiona_importar_con_cobertura_no_existente_tipo_tarifa(String tarifa) throws Throwable {
        importarTarifa.withOutCoberturaNonExistent(tarifa);
        Thread.sleep(1000);
    }

    @Then("^actuario no tiene tipo tarifa (.+) creada con cobertura no existente$")
    public void actuario_no_tiene_tipo_tarifa_creada_con_cobertura_no_existente(String tarifa) throws Throwable {
        Assert.assertTrue(validarImportarTarifa.validarWithOutCoberturaNonExistent(tarifa));
        System.out.println("UPLOAD");
    }

    @When("^actuario presiona importar con datos no existentes tipo tarifa (.+)$")
    public void actuario_presiona_importar_con_datos_no_existentes_tipo_tarifa(String tarifa) throws Throwable {
        importarTarifa.withIncorrectData(tarifa);
        Thread.sleep(1000);

    }

    @Then("^actuario no tiene tipo tarifa (.+) creada con datos no existentes$")
    public void actuario_no_tiene_tipo_tarifa_creada_con_datos_no_existentes(String tarifa) throws Throwable {
        Assert.assertTrue(validarImportarTarifa.validarWithIncorrectData(tarifa));
        System.out.println("UPLOAD");

    }












}

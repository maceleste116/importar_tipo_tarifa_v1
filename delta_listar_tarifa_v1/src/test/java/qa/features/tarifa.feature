Feature: Importar Tipos Tarifa

  Background: Ir al menu tarifa
    Given actuario quiere importar tarifa


  Scenario Outline: Importar tarifa con datos correctos
    When actuario presiona importar tipo tarifa <tarifa>
    Then actuario tiene tipo tarifa <tarifa> creada
    Examples:
      | tarifa    |
      | base      |
      | adicional |
      | factor    |
      | accesoria |

  Scenario Outline: Importar tarifa con formato incorrecto
    When actuario presiona importar con formato incorrecto tipo tarifa <tarifa>
    Then actuario no tiene tipo tarifa <tarifa> creada con formato incorrecto
    Examples:
      | tarifa    |
      | base      |
      | adicional |
      | factor    |
      | accesoria |

  Scenario Outline: Importar tarifa con celdas vacias
    When actuario presiona importar con celdas vacias tipo tarifa <tarifa>
    Then actuario no tiene tipo tarifa <tarifa> creada con celdas vacias
    Examples:
      | tarifa    |
      | base      |
      | adicional |
      | factor    |
      | accesoria |

  Scenario Outline: Importar tarifa con grupo tarifario version
    When actuario presiona importar con con grupo tarifario version tipo tarifa <tarifa>
    Then actuario no tiene tipo tarifa <tarifa> creada con grupo tarifario version
    Examples:
      | tarifa    |
      | base      |
      | adicional |
      | factor    |
      | accesoria |

  Scenario Outline: Importar tarifa con modelo no existente
    When actuario presiona importar con modelo no existente tipo tarifa <tarifa>
    Then actuario no tiene tipo tarifa <tarifa> creada con modelo no existente
    Examples:
      | tarifa    |
      | base      |
      | adicional |
      | factor    |

  Scenario Outline: Importar tarifa con cobertura no existente
    When actuario presiona importar con cobertura no existente tipo tarifa <tarifa>
    Then actuario no tiene tipo tarifa <tarifa> creada con cobertura no existente
    Examples:
      | tarifa    |
      | base      |
      | adicional |
      | factor    |
      | accesoria |

  Scenario Outline: Importar tarifa con datos no existentes
    When actuario presiona importar con datos no existentes tipo tarifa <tarifa>
    Then actuario no tiene tipo tarifa <tarifa> creada con datos no existentes
    Examples:
      | tarifa    |
      | base      |
      | adicional |
      | factor    |
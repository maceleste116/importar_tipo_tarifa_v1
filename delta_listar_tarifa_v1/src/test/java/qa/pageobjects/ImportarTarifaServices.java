package qa.pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ImportarTarifaServices {

    @Autowired
    private WebDriver driver;

    @Autowired
    private WebDriverWait wait;

    @Autowired
    private ImportarTarifaPage importarTarifaPage;

    public void clickOnButtonTarifaBase() {
        this.wait.until(ExpectedConditions.visibilityOf(this.importarTarifaPage.getButtonTarifaBase()));
        this.importarTarifaPage.getButtonTarifaBase().click();
    }

    public void clickOnButtonTarifaAdicional() {
        this.wait.until(ExpectedConditions.visibilityOf(this.importarTarifaPage.getButtonTarifaAdicional()));
        this.importarTarifaPage.getButtonTarifaAdicional().click();
    }

    public void clickOnButtonTarifaFactor() {
        this.wait.until(ExpectedConditions.visibilityOf(this.importarTarifaPage.getButtonTarifaFactor()));
        this.importarTarifaPage.getButtonTarifaFactor().click();
    }

    public void clickOnButtonTarifaAccesoria() {
        this.wait.until(ExpectedConditions.visibilityOf(this.importarTarifaPage.getButtonTarifaAccesoria()));
        this.importarTarifaPage.getButtonTarifaAccesoria().click();
    }

    public void uploadTarifaBase(String tarifa){
        this.importarTarifaPage.getUploadImportarTarifaBase().sendKeys(tarifa);
    }

    public void uploadTarifaAdicional(String tarifa){
        this.importarTarifaPage.getUploadImportarTarifaAdicional().sendKeys(tarifa);
    }

    public void uploadTarifaFactor(String tarifa){
        this.importarTarifaPage.getUploadImportarTarifaFactor().sendKeys(tarifa);
    }

    public void uploadTarifaAccesoria(String tarifa){
        this.importarTarifaPage.getUploadImportarTarifaAccesoria().sendKeys(tarifa);
    }

    public void closeMessage(){
        this.importarTarifaPage.getMessageClose().click();
    }

    public void closeMessageIncorrectFormatFile(){
        this.importarTarifaPage.getMessageCloseIncorrectFormatFile().click();
    }

    public String getMensajeImportarTarifa(){
        this.wait.until(ExpectedConditions.visibilityOf(this.importarTarifaPage.getMensajeImportarTarifa()));
        return this.importarTarifaPage.getMensajeImportarTarifa().getText();
    }

    public String getMensajeImportarTarifaFormatoIncorrecto(){
        this.wait.until(ExpectedConditions.visibilityOf(this.importarTarifaPage.getMensajeImportarTarifaFormatoIncorrecto()));
        return this.importarTarifaPage.getMensajeImportarTarifaFormatoIncorrecto().getText();
    }

}

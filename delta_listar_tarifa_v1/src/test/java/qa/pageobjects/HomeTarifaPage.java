package qa.pageobjects;

import lombok.Getter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Getter
public class HomeTarifaPage extends PageBase {

    @Autowired
    public HomeTarifaPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(id="nabvar-burguer-menu-boton-desplegar")
    private WebElement menuPrincipal;

    @FindBy(id="nabvar-burguer-menu-boton-tarifas")
    private WebElement MenuTarifa ;

    @FindBy(id="nabvar-burguer-menu-tarifas-boton-listar-tarifas")
    private WebElement MenuListarTarifa ;




}

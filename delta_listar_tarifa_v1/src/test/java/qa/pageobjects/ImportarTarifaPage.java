package qa.pageobjects;

import lombok.Getter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Getter
public class ImportarTarifaPage extends PageBase{

    @Autowired
    public ImportarTarifaPage(WebDriver driver){
        super(driver);
    }

    /*@FindBy(xpath="//div[@id='tarifas-boton-TarifaBase']/p/strong")
    private WebElement buttonTarifaBase;

    @FindBy(id="TarifaBase-boton-importar") //no se utiliza el botón
    private WebElement buttonImportarTarifaBase;*/

    @FindBy(id="tarifas-boton-TarifaBase")
    private WebElement buttonTarifaBase;

    @FindBy(id="tarifas-boton-TarifaAdicional")
    private WebElement buttonTarifaAdicional;

    @FindBy(id="tarifas-boton-TarifaFactor")
    private WebElement buttonTarifaFactor;

    @FindBy(id="tarifas-boton-TarifaAccesoria")
    private WebElement buttonTarifaAccesoria;

    @FindBy(id="ImportFile")
    private WebElement uploadImportarTarifaBase;

    @FindBy(id="ImportFile")
    private WebElement uploadImportarTarifaAdicional;

    @FindBy(id="ImportFile")
    private WebElement uploadImportarTarifaFactor;

    @FindBy(id="ImportFile")
    private WebElement uploadImportarTarifaAccesoria;

    @FindBy(id="modal-modal-button-close")
    private WebElement messageClose;

    @FindBy(id="-modal-button-close")
    private WebElement messageCloseIncorrectFormatFile;

    @FindBy(id="modal-mensaje") //La importación fue exitosa
    private WebElement mensajeImportarTarifa;

    @FindBy(id="null-mensaje")
    private WebElement mensajeImportarTarifaFormatoIncorrecto;
     //El excel proporcionado no se corresponde con el esperado para una importación de tarifa base
    //La lista de tarifas no puede ser nula
    //No se puede importar esta Tarifa base, ya que tiene asociado un GrupoTarifarioVersion que no está en edicion



}

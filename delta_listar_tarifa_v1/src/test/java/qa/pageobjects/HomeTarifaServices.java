package qa.pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class HomeTarifaServices {

    @Autowired
    private WebDriver driver;

    @Autowired
    private WebDriverWait wait;

    @Autowired
    private HomeTarifaPage homeTarifaPage;

    public void clickOnMenuPrincipal() {
        this.wait.until(ExpectedConditions.visibilityOf(this.homeTarifaPage.getMenuPrincipal()));
        this.homeTarifaPage.getMenuPrincipal().click();
    }

    public void clickOnMenuTarifa() {
        this.wait.until(ExpectedConditions.visibilityOf(this.homeTarifaPage.getMenuTarifa()));
        this.homeTarifaPage.getMenuTarifa().click();
    }

    public void clickOnListarTarifa() {
        this.wait.until(ExpectedConditions.visibilityOf(this.homeTarifaPage.getMenuListarTarifa()));
        this.homeTarifaPage.getMenuListarTarifa().click();
    }




}

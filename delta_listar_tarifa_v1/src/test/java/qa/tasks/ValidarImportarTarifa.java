package qa.tasks;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import qa.pageobjects.ImportarTarifaServices;

@Component
public class ValidarImportarTarifa {

    @Autowired
    private ImportarTarifaServices importarTarifaServices;

    public boolean validarWithCorrectFile(String tarifa) throws InterruptedException {
        if (tarifa.equals("base")) {
            String mensajeBase = importarTarifaServices.getMensajeImportarTarifa();
            boolean contieneMensajeBase = mensajeBase.contains("La importación fue exitosa");
            importarTarifaServices.closeMessage();
            return contieneMensajeBase;
        } else if (tarifa.equals("adicional")) {
            String mensajeAdicional = importarTarifaServices.getMensajeImportarTarifa();
            boolean contieneMensajeAdicional = mensajeAdicional.contains("La importación fue exitosa");
            importarTarifaServices.closeMessage();
            return contieneMensajeAdicional;
        } else if (tarifa.equals("factor")) {
            String mensajeFactor = importarTarifaServices.getMensajeImportarTarifa();
            boolean contieneMensajeFactor = mensajeFactor.contains("La importación fue exitosa");
            importarTarifaServices.closeMessage();
            return contieneMensajeFactor;
        } else if (tarifa.equals("accesoria")) {
            String mensajeAccesoria = importarTarifaServices.getMensajeImportarTarifa();
            boolean contieneMensajeAccesoria = mensajeAccesoria.contains("La importación fue exitosa");
            importarTarifaServices.closeMessage();
            return contieneMensajeAccesoria;
        } else {
            System.out.println("La tarifa ingresada es incorrecta" + tarifa);
            return false;
        }

    }

    public boolean validarWithIncorrectFormatFile(String tarifa) throws InterruptedException {
        if (tarifa.equals("base")) {
            String mensajeBase = importarTarifaServices.getMensajeImportarTarifaFormatoIncorrecto();
            boolean contieneMensajeBase = mensajeBase.contains("El excel proporcionado no se corresponde con el esperado para una importación de tarifa");
            importarTarifaServices.closeMessageIncorrectFormatFile();
            return contieneMensajeBase;
        } else if (tarifa.equals("adicional")) {
            String mensajeAdicional = importarTarifaServices.getMensajeImportarTarifaFormatoIncorrecto();
            boolean contieneMensajeAdicional = mensajeAdicional.contains("El excel proporcionado no se corresponde con el esperado para una importación de tarifa");
            importarTarifaServices.closeMessageIncorrectFormatFile();
            return contieneMensajeAdicional;
        } else if (tarifa.equals("factor")) {
            String mensajeFactor = importarTarifaServices.getMensajeImportarTarifaFormatoIncorrecto();
            boolean contieneMensajeFactor = mensajeFactor.contains("El excel proporcionado no se corresponde con el esperado para una importación de tarifa");
            importarTarifaServices.closeMessageIncorrectFormatFile();
            return contieneMensajeFactor;
        } else if (tarifa.equals("accesoria")) {
            String mensajeAccesoria = importarTarifaServices.getMensajeImportarTarifaFormatoIncorrecto();
            boolean contieneMensajeAccesoria = mensajeAccesoria.contains("El excel proporcionado no se corresponde con el esperado para una importación de tarifa");
            importarTarifaServices.closeMessageIncorrectFormatFile();
            return contieneMensajeAccesoria;
        } else {
            System.out.println("La tarifa ingresada es incorrecta" + tarifa);
            return false;
        }

    }

    public boolean validarEmptyFieldsFile(String tarifa) throws InterruptedException {
        if (tarifa.equals("base")) {
            String mensajeBase = importarTarifaServices.getMensajeImportarTarifaFormatoIncorrecto();
            boolean contieneMensajeBase = mensajeBase.contains("La lista de tarifas no puede ser nula");
            importarTarifaServices.closeMessageIncorrectFormatFile();
            return contieneMensajeBase;
        } else if (tarifa.equals("adicional")) {
            String mensajeAdicional = importarTarifaServices.getMensajeImportarTarifaFormatoIncorrecto();
            boolean contieneMensajeAdicional = mensajeAdicional.contains("La lista de tarifas no puede ser nula");
            importarTarifaServices.closeMessageIncorrectFormatFile();
            return contieneMensajeAdicional;
        } else if (tarifa.equals("factor")) {
            String mensajeFactor = importarTarifaServices.getMensajeImportarTarifaFormatoIncorrecto();
            boolean contieneMensajeFactor = mensajeFactor.contains("La lista de tarifas no puede ser nula");
            importarTarifaServices.closeMessageIncorrectFormatFile();
            return contieneMensajeFactor;
        } else if (tarifa.equals("accesoria")) {
            String mensajeAccesoria = importarTarifaServices.getMensajeImportarTarifaFormatoIncorrecto();
            boolean contieneMensajeAccesoria = mensajeAccesoria.contains("La lista de tarifas no puede ser nula");
            importarTarifaServices.closeMessageIncorrectFormatFile();
            return contieneMensajeAccesoria;
        } else {
            System.out.println("La tarifa ingresada es incorrecta" + tarifa);
            return false;
        }
    }

        public boolean validarWithRateGroupVersion(String tarifa) throws InterruptedException {
            if (tarifa.equals("base")) {
                String mensajeBase = importarTarifaServices.getMensajeImportarTarifaFormatoIncorrecto();
                boolean contieneMensajeBase = mensajeBase.contains("ya que tiene asociado un GrupoTarifarioVersion que no está en edicion");
                importarTarifaServices.closeMessageIncorrectFormatFile();
                return contieneMensajeBase;
            } else if (tarifa.equals("adicional")) {
                String mensajeAdicional = importarTarifaServices.getMensajeImportarTarifaFormatoIncorrecto();
                boolean contieneMensajeAdicional = mensajeAdicional.contains("ya que tiene asociado un GrupoTarifarioVersion que no está en edicion");
                importarTarifaServices.closeMessageIncorrectFormatFile();
                return contieneMensajeAdicional;
            } else if (tarifa.equals("factor")) {
                String mensajeFactor = importarTarifaServices.getMensajeImportarTarifaFormatoIncorrecto();
                boolean contieneMensajeFactor = mensajeFactor.contains("ya que tiene asociado un GrupoTarifarioVersion que no está en edicion");
                importarTarifaServices.closeMessageIncorrectFormatFile();
                return contieneMensajeFactor;
            } else if (tarifa.equals("accesoria")) {
                String mensajeAccesoria = importarTarifaServices.getMensajeImportarTarifaFormatoIncorrecto();
                boolean contieneMensajeAccesoria = mensajeAccesoria.contains("ya que tiene asociado un GrupoTarifarioVersion que no está en edicion");
                importarTarifaServices.closeMessageIncorrectFormatFile();
                return contieneMensajeAccesoria;
            } else {
                System.out.println("La tarifa ingresada es incorrecta" + tarifa);
                return false;
            }
        }

    public boolean validarWithOutModeloNonExistent(String tarifa) throws InterruptedException {
        if (tarifa.equals("base")) {
            String mensajeBase = importarTarifaServices.getMensajeImportarTarifaFormatoIncorrecto();
            boolean contieneMensajeBase = mensajeBase.contains("Código de modelo no válido");
            importarTarifaServices.closeMessageIncorrectFormatFile();
            return contieneMensajeBase;
        } else if (tarifa.equals("adicional")) {
            String mensajeAdicional = importarTarifaServices.getMensajeImportarTarifaFormatoIncorrecto();
            boolean contieneMensajeAdicional = mensajeAdicional.contains("Código de modelo no válido");
            importarTarifaServices.closeMessageIncorrectFormatFile();
            return contieneMensajeAdicional;
        } else if (tarifa.equals("factor")) {
            String mensajeFactor = importarTarifaServices.getMensajeImportarTarifaFormatoIncorrecto();
            boolean contieneMensajeFactor = mensajeFactor.contains("Código de modelo no válido");
            importarTarifaServices.closeMessageIncorrectFormatFile();
            return contieneMensajeFactor;

        } else {
            System.out.println("La tarifa ingresada es incorrecta" + tarifa);
            return false;
        }
    }


    public boolean validarWithOutCoberturaNonExistent(String tarifa) throws InterruptedException {
        if (tarifa.equals("base")) {
            String mensajeBase = importarTarifaServices.getMensajeImportarTarifaFormatoIncorrecto();
            boolean contieneMensajeBase = mensajeBase.contains("Código de cobertura no válido");
            importarTarifaServices.closeMessageIncorrectFormatFile();
            return contieneMensajeBase;
        } else if (tarifa.equals("adicional")) {
            String mensajeAdicional = importarTarifaServices.getMensajeImportarTarifaFormatoIncorrecto();
            boolean contieneMensajeAdicional = mensajeAdicional.contains("Código de cobertura no válido");
            importarTarifaServices.closeMessageIncorrectFormatFile();
            return contieneMensajeAdicional;
        } else if (tarifa.equals("factor")) {
            String mensajeFactor = importarTarifaServices.getMensajeImportarTarifaFormatoIncorrecto();
            boolean contieneMensajeFactor = mensajeFactor.contains("Código de cobertura no válido");
            importarTarifaServices.closeMessageIncorrectFormatFile();
            return contieneMensajeFactor;
        } else if (tarifa.equals("accesoria")) {
            String mensajeAccesoria = importarTarifaServices.getMensajeImportarTarifaFormatoIncorrecto();
            boolean contieneMensajeAccesoria = mensajeAccesoria.contains("Código de cobertura no válido");
            importarTarifaServices.closeMessageIncorrectFormatFile();
            return contieneMensajeAccesoria;
        } else {
            System.out.println("La tarifa ingresada es incorrecta" + tarifa);
            return false;
        }
    }
    public boolean validarWithIncorrectData(String tarifa) throws InterruptedException {
        if (tarifa.equals("base")) {
            String mensajeBase = importarTarifaServices.getMensajeImportarTarifaFormatoIncorrecto();
            boolean contieneMensajeBase = mensajeBase.contains("Existen datos incorrectos");
            importarTarifaServices.closeMessageIncorrectFormatFile();
            return contieneMensajeBase;
        } else if (tarifa.equals("adicional")) {
            String mensajeAdicional = importarTarifaServices.getMensajeImportarTarifaFormatoIncorrecto();
            boolean contieneMensajeAdicional = mensajeAdicional.contains("Existen datos incorrectos");
            importarTarifaServices.closeMessageIncorrectFormatFile();
            return contieneMensajeAdicional;
        } else if (tarifa.equals("factor")) {
            String mensajeFactor = importarTarifaServices.getMensajeImportarTarifaFormatoIncorrecto();
            boolean contieneMensajeFactor = mensajeFactor.contains("Existen datos incorrectos");
            importarTarifaServices.closeMessageIncorrectFormatFile();
            return contieneMensajeFactor;
        } else {
            System.out.println("La tarifa ingresada es incorrecta" + tarifa);
            return false;
        }
    }
}

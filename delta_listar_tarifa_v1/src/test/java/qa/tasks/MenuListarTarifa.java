package qa.tasks;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import qa.pageobjects.HomeTarifaServices;

@Component
public class MenuListarTarifa {
    @Autowired
    private HomeTarifaServices homeTarifaServices;

    public void irMenuListarTarifa(){
        homeTarifaServices.clickOnMenuPrincipal();
        homeTarifaServices.clickOnMenuTarifa();
        homeTarifaServices.clickOnListarTarifa();
    }
}

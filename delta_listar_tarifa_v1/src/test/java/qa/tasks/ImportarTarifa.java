package qa.tasks;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import qa.pageobjects.HomeTarifaServices;
import qa.pageobjects.ImportarTarifaServices;

@Component
public class ImportarTarifa {

    @Autowired
    private HomeTarifaServices homeTarifaServices;

    @Autowired
    private ImportarTarifaServices importarTarifaServices;

    public void withCorrectFile (String tarifa) throws InterruptedException {
        if(tarifa.equals("base")){
            importarTarifaServices.clickOnButtonTarifaBase();
            importarTarifaServices.uploadTarifaBase("C:/Users/berdinas/IdeaProjects/delta_sprint_6/tarifas/base/importar_tarifa_base.xlsx");
        } else if(tarifa.equals("adicional")){
            importarTarifaServices.clickOnButtonTarifaAdicional();
            importarTarifaServices.uploadTarifaAdicional("C:/Users/berdinas/IdeaProjects/delta_sprint_6/tarifas/adicional/importar_tarifa_adicional.xlsx");
        } else if(tarifa.equals("factor")){
            importarTarifaServices.clickOnButtonTarifaFactor();
            importarTarifaServices.uploadTarifaFactor("C:/Users/berdinas/IdeaProjects/delta_sprint_6/tarifas/factor/importar_tarifa_factor.xlsx");

        } else if(tarifa.equals("accesoria")) {
            importarTarifaServices.clickOnButtonTarifaAccesoria();
            importarTarifaServices.uploadTarifaAccesoria("C:/Users/berdinas/IdeaProjects/delta_sprint_6/tarifas/accesoria/importar_tarifa_accesoria.xlsx");
        }
        else {
            System.out.println("La tarifa ingresada es incorrecta" + tarifa);
        }
    }

    public void withIncorrectFormatFile (String tarifa) throws InterruptedException {
        if(tarifa.equals("base")){
            importarTarifaServices.clickOnButtonTarifaBase();
            importarTarifaServices.uploadTarifaBase("C:/Users/berdinas/IdeaProjects/delta_sprint_6/tarifas/base/importar_tarifa_base_formato_incorrecto.xlsx");

        } else if(tarifa.equals("adicional")){
            importarTarifaServices.clickOnButtonTarifaAdicional();
            importarTarifaServices.uploadTarifaAdicional("C:/Users/berdinas/IdeaProjects/delta_sprint_6/tarifas/adicional/importar_tarifa_adicional_formato_incorrecto.xlsx");
        } else if(tarifa.equals("factor")){
            importarTarifaServices.clickOnButtonTarifaFactor();
            importarTarifaServices.uploadTarifaFactor("C:/Users/berdinas/IdeaProjects/delta_sprint_6/tarifas/factor/importar_tarifa_factor_formato_incorrecto.xlsx");

        } else if(tarifa.equals("accesoria")) {
            importarTarifaServices.clickOnButtonTarifaAccesoria();
            importarTarifaServices.uploadTarifaAccesoria("C:/Users/berdinas/IdeaProjects/delta_sprint_6/tarifas/accesoria/importar_tarifa_accesoria_formato_incorrecto.xlsx");
        }
        else {
            System.out.println("La tarifa ingresada es incorrecta" + tarifa);
        }
    }

    public void withEmptyFieldsFile (String tarifa) throws InterruptedException {
        if(tarifa.equals("base")){
            importarTarifaServices.clickOnButtonTarifaBase();
            importarTarifaServices.uploadTarifaBase("C:/Users/berdinas/IdeaProjects/delta_sprint_6/tarifas/base/importar_tarifa_base_celdas_vacias.xlsx");

        } else if(tarifa.equals("adicional")){
            importarTarifaServices.clickOnButtonTarifaAdicional();
            importarTarifaServices.uploadTarifaAdicional("C:/Users/berdinas/IdeaProjects/delta_sprint_6/tarifas/adicional/importar_tarifa_adicional_celdas_vacias.xlsx");
        } else if(tarifa.equals("factor")){
            importarTarifaServices.clickOnButtonTarifaFactor();
            importarTarifaServices.uploadTarifaFactor("C:/Users/berdinas/IdeaProjects/delta_sprint_6/tarifas/factor/importar_tarifa_factor_celdas_vacias.xlsx");

        } else if(tarifa.equals("accesoria")) {
            importarTarifaServices.clickOnButtonTarifaAccesoria();
            importarTarifaServices.uploadTarifaAccesoria("C:/Users/berdinas/IdeaProjects/delta_sprint_6/tarifas/accesoria/importar_tarifa_accesoria_celdas_vacias.xlsx");
        }
        else {
            System.out.println("La tarifa ingresada es incorrecta" + tarifa);
        }
    }

    public void withRateGroupVersion(String tarifa) throws InterruptedException {
        if(tarifa.equals("base")){
            importarTarifaServices.clickOnButtonTarifaBase();
            importarTarifaServices.uploadTarifaBase("C:/Users/berdinas/IdeaProjects/delta_sprint_6/tarifas/base/importar_tarifa_base_version.xlsx");

        } else if(tarifa.equals("adicional")){
            importarTarifaServices.clickOnButtonTarifaAdicional();
            importarTarifaServices.uploadTarifaAdicional("C:/Users/berdinas/IdeaProjects/delta_sprint_6/tarifas/adicional/importar_tarifa_adicional_version.xlsx");
        } else if(tarifa.equals("factor")){
            importarTarifaServices.clickOnButtonTarifaFactor();
            importarTarifaServices.uploadTarifaFactor("C:/Users/berdinas/IdeaProjects/delta_sprint_6/tarifas/factor/importar_tarifa_factor_version.xlsx");

        } else if(tarifa.equals("accesoria")) {
            importarTarifaServices.clickOnButtonTarifaAccesoria();
            importarTarifaServices.uploadTarifaAccesoria("C:/Users/berdinas/IdeaProjects/delta_sprint_6/tarifas/accesoria/importar_tarifa_accesoria_version.xlsx");
        }
        else {
            System.out.println("La tarifa ingresada es incorrecta" + tarifa);
        }
    }

    public void withOutModeloNonExistent(String tarifa) throws InterruptedException {
        if(tarifa.equals("base")){
            importarTarifaServices.clickOnButtonTarifaBase();
            importarTarifaServices.uploadTarifaBase("C:/Users/berdinas/IdeaProjects/delta_sprint_6/tarifas/base/importar_tarifa_base_modelo.xlsx");

        } else if(tarifa.equals("adicional")){
            importarTarifaServices.clickOnButtonTarifaAdicional();
            importarTarifaServices.uploadTarifaAdicional("C:/Users/berdinas/IdeaProjects/delta_sprint_6/tarifas/adicional/importar_tarifa_adicional_modelo.xlsx");
        } else if(tarifa.equals("factor")){
            importarTarifaServices.clickOnButtonTarifaFactor();
            importarTarifaServices.uploadTarifaFactor("C:/Users/berdinas/IdeaProjects/delta_sprint_6/tarifas/factor/importar_tarifa_factor_modelo.xlsx");

        } else {
            System.out.println("La tarifa ingresada es incorrecta" + tarifa);
        }
    }

    public void withOutCoberturaNonExistent(String tarifa) throws InterruptedException {

        if(tarifa.equals("base")){
            importarTarifaServices.clickOnButtonTarifaBase();
            importarTarifaServices.uploadTarifaBase("C:/Users/berdinas/IdeaProjects/delta_sprint_6/tarifas/base/importar_tarifa_base_cobertura.xlsx");
        } else if(tarifa.equals("adicional")){
            importarTarifaServices.clickOnButtonTarifaAdicional();
            importarTarifaServices.uploadTarifaAdicional("C:/Users/berdinas/IdeaProjects/delta_sprint_6/tarifas/adicional/importar_tarifa_adicional_cobertura.xlsx");
        } else if(tarifa.equals("factor")){
            importarTarifaServices.clickOnButtonTarifaFactor();
            importarTarifaServices.uploadTarifaFactor("C:/Users/berdinas/IdeaProjects/delta_sprint_6/tarifas/factor/importar_tarifa_factor_cobertura.xlsx");
        } else if(tarifa.equals("accesoria")) {
            importarTarifaServices.clickOnButtonTarifaAccesoria();
            importarTarifaServices.uploadTarifaAccesoria("C:/Users/berdinas/IdeaProjects/delta_sprint_6/tarifas/accesoria/importar_tarifa_accesoria_cobertura.xlsx");
        }
        else {
            System.out.println("La tarifa ingresada es incorrecta" + tarifa);
        }
    }

    public void withIncorrectData(String tarifa) throws InterruptedException {

        if(tarifa.equals("base")){
            importarTarifaServices.clickOnButtonTarifaBase();
            importarTarifaServices.uploadTarifaBase("C:/Users/berdinas/IdeaProjects/delta_sprint_6/tarifas/base/importar_tarifa_base_inconsistencia_todos.xlsx");
        } else if(tarifa.equals("adicional")){
            importarTarifaServices.clickOnButtonTarifaAdicional();
            importarTarifaServices.uploadTarifaAdicional("C:/Users/berdinas/IdeaProjects/delta_sprint_6/tarifas/adicional/importar_tarifa_adicional_inconsistencia_todos.xlsx");
        } else if(tarifa.equals("factor")){
            importarTarifaServices.clickOnButtonTarifaFactor();
            importarTarifaServices.uploadTarifaFactor("C:/Users/berdinas/IdeaProjects/delta_sprint_6/tarifas/factor/importar_tarifa_factor_inconsistencia_todos.xlsx");
        } else {
            System.out.println("La tarifa ingresada es incorrecta" + tarifa);
        }
    }
}
